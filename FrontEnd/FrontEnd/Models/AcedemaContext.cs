﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace FrontEnd.Models
{
    public partial class AcedemaContext : DbContext
    {
        public AcedemaContext()
        {
        }

        public AcedemaContext(DbContextOptions<AcedemaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cursos> Cursos { get; set; }
        public virtual DbSet<Eventos> Eventos { get; set; }
        public virtual DbSet<Matricula> Matricula { get; set; }
        public virtual DbSet<Usuarios> Usuarios { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=LAPTOP-GS9S6OHI\\SQLEXPRESS;Database=Acedema;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cursos>(entity =>
            {
                entity.HasKey(e => e.Idcurso)
                    .HasName("PK_CURSO");

                entity.ToTable("CURSOS");

                entity.Property(e => e.Idcurso).HasColumnName("IDCURSO");

                entity.Property(e => e.Fecha)
                    .HasColumnName("FECHA")
                    .HasColumnType("date");

                entity.Property(e => e.Nombre)
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Eventos>(entity =>
            {
                entity.HasKey(e => e.Idevento);

                entity.ToTable("EVENTOS");

                entity.Property(e => e.Idevento).HasColumnName("IDEVENTO");

                entity.Property(e => e.Descripcion)
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Fecha)
                    .HasColumnName("FECHA")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<Matricula>(entity =>
            {
                entity.HasKey(e => e.Idmatricula);

                entity.ToTable("MATRICULA");

                entity.Property(e => e.Idmatricula).HasColumnName("IDMATRICULA");

                entity.Property(e => e.Idcurso).HasColumnName("IDCURSO");

                entity.Property(e => e.Idusuario).HasColumnName("IDUSUARIO");

                entity.HasOne(d => d.IdcursoNavigation)
                    .WithMany(p => p.Matricula)
                    .HasForeignKey(d => d.Idcurso)
                    .HasConstraintName("FK_MATRICULACURSO");

                entity.HasOne(d => d.IdusuarioNavigation)
                    .WithMany(p => p.Matricula)
                    .HasForeignKey(d => d.Idusuario)
                    .HasConstraintName("FK_MATRICULAUSER");
            });

            modelBuilder.Entity<Usuarios>(entity =>
            {
                entity.HasKey(e => e.Idusuario);

                entity.ToTable("USUARIOS");

                entity.Property(e => e.Idusuario).HasColumnName("IDUSUARIO");

                entity.Property(e => e.Apellidos)
                    .HasColumnName("APELLIDOS")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Contrasena)
                    .HasColumnName("CONTRASENA")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Correo)
                    .HasColumnName("CORREO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Rol)
                    .HasColumnName("ROL")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Telefono)
                    .HasColumnName("TELEFONO")
                    .HasColumnType("numeric(8, 0)");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
