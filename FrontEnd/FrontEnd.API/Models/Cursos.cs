﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace FrontEnd.API.Models
{
    public partial class Cursos
    {
        public Cursos()
        {
            Matricula = new HashSet<Matricula>();
        }

        public int Idcurso { get; set; }
        public string Nombre { get; set; }
        public DateTime? Fecha { get; set; }

        public virtual ICollection<Matricula> Matricula { get; set; }
    }
}
