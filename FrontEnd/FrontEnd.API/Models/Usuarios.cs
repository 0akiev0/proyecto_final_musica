﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace FrontEnd.API.Models
{
    public partial class Usuarios
    {
        public Usuarios()
        {
            Matricula = new HashSet<Matricula>();
        }

        public int Idusuario { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Correo { get; set; }
        public decimal? Telefono { get; set; }
        public string Contrasena { get; set; }
        public string Rol { get; set; }

        public virtual ICollection<Matricula> Matricula { get; set; }
    }
}
