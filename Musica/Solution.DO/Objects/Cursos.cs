﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Solution.DO.Objects
{
    public class Cursos
    {
        public Cursos()
        {
            Matricula = new HashSet<Matricula>();
        }

        public int Idcurso { get; set; }
        public string Nombre { get; set; }
        public DateTime? Fecha { get; set; }

        public virtual ICollection<Matricula> Matricula { get; set; }
    }
}
