﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Solution.DO.Objects
{
    public class Eventos
    {
        public int Idevento { get; set; }
        public string Descripcion { get; set; }
        public DateTime? Fecha { get; set; }
    }
}
