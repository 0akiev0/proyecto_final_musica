﻿using Solution.DAL.EF;
using Solution.DO.Interfaces;
using Solution.DO.Objects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Solution.BS
{
    public class Usuario : ICRUD<Usuarios>
    {
        private SolutionDBContext dbContext = null;

        public Usuario(SolutionDBContext context)
        {
            this.dbContext = context;
        }

        public void Delete(Usuarios t)
        {
            new DAL.Usuario(dbContext).Delete(t);
        }

        public IEnumerable<Usuarios> GetAll()
        {
            return new DAL.Usuario(dbContext).GetAll();
        }

        public Usuarios GetOneById(int id)
        {
            return new DAL.Usuario(dbContext).GetOneById(id);
        }

        public void Insert(Usuarios t)
        {
            new DAL.Usuario(dbContext).Insert(t);
        }

        public void Update(Usuarios t)
        {
            new DAL.Usuario(dbContext).Update(t);
        }
    }
}
