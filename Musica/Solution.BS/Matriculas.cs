﻿using Solution.DAL.EF;
using Solution.DO.Interfaces;
using data = Solution.DO.Objects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Solution.BS
{
    public class Matricula : ICRUD<data.Matricula>
    {
        private SolutionDBContext _repo = null;

        public Matricula(SolutionDBContext solutionDBContext)
        {
            _repo = solutionDBContext;
        }

        public void Delete(data.Matricula t)
        {
            new DAL.Matricula(_repo).Delete(t);
        }

        public IEnumerable<data.Matricula> GetAll()
        {
            return new DAL.Matricula(_repo).GetAll();
        }

        public data.Matricula GetOneById(int id)
        {

            return new DAL.Matricula(_repo).GetOneById(id);
        }

        public void Insert(data.Matricula t)
        {

            new DAL.Matricula(_repo).Insert(t);
        }

        public void Update(data.Matricula t)
        {

            new DAL.Matricula(_repo).Update(t);
        }

        public async Task<IEnumerable<data.Matricula>> GetAllInclude()
        {
            return await new DAL.Matricula(_repo).GetAllInclude();
        }

        public async Task<data.Matricula> GetOneByIdInclude(int id)
        {
            return await new DAL.Matricula(_repo).GetOneByIdInclude(id);
        }
    }
}
