﻿using Solution.DAL.EF;
using Solution.DO.Interfaces;
using Solution.DO.Objects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Solution.BS
{
    public class Evento : ICRUD<Eventos>
    {
        private SolutionDBContext dbContext = null;

        public Evento(SolutionDBContext context)
        {
            this.dbContext = context;
        }

        public void Delete(Eventos t)
        {
            new DAL.Evento(dbContext).Delete(t);
        }

        public IEnumerable<Eventos> GetAll()
        {
            return new DAL.Evento(dbContext).GetAll();
        }

        public Eventos GetOneById(int id)
        {
            return new DAL.Evento(dbContext).GetOneById(id);
        }

        public void Insert(Eventos t)
        {
            new DAL.Evento(dbContext).Insert(t);
        }

        public void Update(Eventos t)
        {
            new DAL.Evento(dbContext).Update(t);
        }
    }
}
