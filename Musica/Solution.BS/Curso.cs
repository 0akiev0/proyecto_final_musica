﻿using Solution.DAL.EF;
using Solution.DO.Interfaces;
using Solution.DO.Objects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Solution.BS
{
    public class Curso : ICRUD<Cursos>
    {
        private SolutionDBContext dbContext = null;

        public Curso(SolutionDBContext context)
        {
            this.dbContext = context;
        }

        public void Delete(Cursos t)
        {
            new DAL.Curso(dbContext).Delete(t);
        }

        public IEnumerable<Cursos> GetAll()
        {
            return new DAL.Curso(dbContext).GetAll();
        }

        public Cursos GetOneById(int id)
        {
            return new DAL.Curso(dbContext).GetOneById(id);
        }

        public void Insert(Cursos t)
        {
            new DAL.Curso(dbContext).Insert(t);
        }

        public void Update(Cursos t)
        {
            new DAL.Curso(dbContext).Update(t);
        }
    }
}
