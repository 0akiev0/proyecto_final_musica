﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Musica.W.Models
{
    public partial class Matricula
    {
        public int Idmatricula { get; set; }
        public int? Idcurso { get; set; }
        public int? Idusuario { get; set; }

        public virtual Cursos IdcursoNavigation { get; set; }
        public virtual Usuarios IdusuarioNavigation { get; set; }
    }
}
