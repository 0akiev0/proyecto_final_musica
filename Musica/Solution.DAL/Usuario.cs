﻿using Solution.DAL.EF;
using Solution.DAL.Repository;
using Solution.DO.Interfaces;
using Solution.DO.Objects;
using System;
using System.Collections.Generic;
using System.Text;
using data = Solution.DO;

namespace Solution.DAL
{
    public class Usuario : ICRUD<data.Objects.Usuarios>
    {
        private Repository<data.Objects.Usuarios> _repo = null;

        public Usuario(SolutionDBContext solutionDbContext)
        {
            _repo = new Repository<data.Objects.Usuarios>(solutionDbContext);
        }
        public void Delete(Usuarios t)
        {
            _repo.Delete(t);
            _repo.Commit();
        }

        public IEnumerable<Usuarios> GetAll()
        {
            return _repo.GetAll();
        }

        public Usuarios GetOneById(int id)
        {
            return _repo.GetOneById(id);
        }

        public void Insert(Usuarios t)
        {
            try
            {
                _repo.Insert(t);
                _repo.Commit();
            }
            catch (Exception ee)
            {

            }
        }

        public void Update(Usuarios t)
        {
            _repo.Update(t);
            _repo.Commit();
        }
    }
}
