﻿using Solution.DAL.EF;
using Solution.DAL.Repository;
using Solution.DO.Interfaces;
using Solution.DO.Objects;
using System;
using System.Collections.Generic;
using System.Text;
using data = Solution.DO;

namespace Solution.DAL
{
    public class Evento : ICRUD<data.Objects.Eventos>
    {
        private Repository<data.Objects.Eventos> _repo = null;

        public Evento(SolutionDBContext solutionDbContext)
        {
            _repo = new Repository<data.Objects.Eventos>(solutionDbContext);
        }

        public void Delete(Eventos t)
        {
            _repo.Delete(t);
            _repo.Commit();
        }

        public IEnumerable<Eventos> GetAll()
        {
            return _repo.GetAll();
        }

        public Eventos GetOneById(int id)
        {
            return _repo.GetOneById(id);
        }

        public void Insert(Eventos t)
        {
            try
            {
                _repo.Insert(t);
                _repo.Commit();
            }
            catch (Exception ee)
            {

            }
        }

        public void Update(Eventos t)
        {
            _repo.Update(t);
            _repo.Commit();
        }
    }
}
