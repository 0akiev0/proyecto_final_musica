﻿using Solution.DAL.EF;
using Solution.DAL.Repository;
using Solution.DO.Interfaces;
using Solution.DO.Objects;
using System;
using System.Collections.Generic;
using System.Text;
using data = Solution.DO;

namespace Solution.DAL
{
    public class Curso : ICRUD<data.Objects.Cursos>
    {
        private Repository<data.Objects.Cursos> _repo = null;

        public Curso(SolutionDBContext solutionDbContext)
        {
            _repo = new Repository<data.Objects.Cursos>(solutionDbContext);
        }
        public void Delete(Cursos t)
        {
            _repo.Delete(t);
            _repo.Commit();
        }

        public IEnumerable<Cursos> GetAll()
        {
            return _repo.GetAll();
        }

        public Cursos GetOneById(int id)
        {
            return _repo.GetOneById(id);
        }

        public void Insert(Cursos t)
        {
            try
            {
                _repo.Insert(t);
                _repo.Commit();
            }
            catch (Exception ee)
            {

            }
        }

        public void Update(Cursos t)
        {
            _repo.Update(t);
            _repo.Commit();
        }
    }
}
