﻿using Solution.DAL.EF;
using Solution.DO.Interfaces;
using Solution.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using data = Solution.DO.Objects;
using System.Threading.Tasks;

namespace Solution.DAL
{
    public class Matricula : ICRUD<data.Matricula>
    {
        private RepositoryMatricula _repo = null;

        public Matricula(SolutionDBContext solutionDbContext)
        {
            _repo = new RepositoryMatricula(solutionDbContext);
        }

        public void Delete(data.Matricula t)
        {
            _repo.Delete(t);
            _repo.Commit(); 
        }

        public IEnumerable<data.Matricula> GetAll()
        {
            return _repo.GetAll();
        }

        public data.Matricula GetOneById(int id)
        {
            return _repo.GetOneById(id);
        }

        public void Insert(data.Matricula t)
        {
            _repo.Insert(t);
            _repo.Commit();
        }

        public void Update(data.Matricula t)
        {
            _repo.Update(t);
            _repo.Commit();
        }

        public async Task<IEnumerable<data.Matricula>> GetAllInclude()
        {
            return await _repo.GetAllWithAsync();
        }

        public async Task<data.Matricula> GetOneByIdInclude(int id)
        {
            return await _repo.GetOneByIdWithAsync(id);
        }
    }
}
