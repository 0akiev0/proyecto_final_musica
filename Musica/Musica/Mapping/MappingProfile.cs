﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using data = Solution.DO.Objects;

namespace Musica.Mapping
{
    public class MappingProfile:Profile
    {
        public MappingProfile()
        {
            CreateMap<data.Eventos, DataModels.Eventos>().ReverseMap();
            CreateMap<data.Usuarios, DataModels.Usuarios>().ReverseMap();
            CreateMap<data.Cursos, DataModels.Cursos>().ReverseMap();
            CreateMap<data.Matricula, DataModels.Matricula>().ReverseMap();
        }
    }
}
