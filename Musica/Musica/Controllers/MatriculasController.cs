﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Solution.DAL.EF;
using data = Solution.DO.Objects;
using AutoMapper;


namespace Solution.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MatriculaController : ControllerBase
    {
        private readonly SolutionDBContext _context;
        private readonly IMapper _mapper;

        public MatriculaController(SolutionDBContext context, IMapper mapper)
        {
            this._mapper = mapper;
            _context = context;
        }

        // GET: api/Matricula
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Musica.DataModels.Matricula>>> GetMatricula()
        {
            // carga de datos
            var aux = await new BS.Matricula(_context).GetAllInclude();

            //implementacion del automapper
            var mappaux = _mapper.Map<IEnumerable<data.Matricula>, IEnumerable<Musica.DataModels.Matricula>>(aux).ToList();
            return mappaux;
        }

        // GET: api/Matricula/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Musica.DataModels.Matricula>> GetMatricula(int id)
        {
            // carga de datos
            var aux = await new BS.Matricula(_context).GetOneByIdInclude(id);

            //implementacion del automapper
            var mappaux = _mapper.Map<data.Matricula, Musica.DataModels.Matricula>(aux);
            if (mappaux == null)
            {
                return NotFound();
            }

            return mappaux;
        }

        // PUT: api/Matricula/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMatricula(int id, Musica.DataModels.Matricula Matricula)
        {
            if (id != Matricula.Idmatricula)
            {
                return BadRequest();
            }

            try
            {
                var mappaux = _mapper.Map<Musica.DataModels.Matricula, data.Matricula>(Matricula);
                new BS.Matricula(_context).Update(mappaux);
            }
            catch (Exception ee)
            {
                if (!MatriculaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Matricula
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Musica.DataModels.Matricula>> PostMatricula(Musica.DataModels.Matricula Matricula)
        {
            var mappaux = _mapper.Map<Musica.DataModels.Matricula, data.Matricula>(Matricula);
            new BS.Matricula(_context).Insert(mappaux);

            return CreatedAtAction("GetMatricula", new { id = Matricula.Idmatricula }, Matricula);
        }

        // DELETE: api/Matricula/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Musica.DataModels.Matricula>> DeleteMatricula(int id)
        {
            var Matricula = new BS.Matricula(_context).GetOneById(id);
            if (Matricula == null)
            {
                return NotFound();
            }

            new BS.Matricula(_context).Delete(Matricula);
            var mappaux = _mapper.Map<data.Matricula, Musica.DataModels.Matricula>(Matricula);


            return mappaux;
        }

        private bool MatriculaExists(int id)
        {
            return (new BS.Matricula(_context).GetOneById(id) != null);
        }
    }
}
