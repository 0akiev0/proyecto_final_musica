﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Solution.DAL.EF;
using data = Solution.DO.Objects;
using AutoMapper;

namespace Musica.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        private readonly SolutionDBContext _context;
        private readonly IMapper _mapper;

        public UsuariosController(SolutionDBContext context, IMapper mapper)
        {
            _context = context;
            this._mapper = mapper;
        }

        // GET: api/Usuarios
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DataModels.Usuarios>>> GetUsuarios()
        {
            // carga de datos
            var aux = new Solution.BS.Usuario(_context).GetAll().ToList();

            //implementacion del automapper
            var mappaux = _mapper.Map<IEnumerable<data.Usuarios>, IEnumerable<DataModels.Usuarios>>(aux).ToList();
            return mappaux.ToList();
        }

        // GET: api/Usuarios/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DataModels.Usuarios>> GetUsuarios(int id)
        {
            var Usuarios = new Solution.BS.Usuario(_context).GetOneById(id);
            //implementacion del automapper
            var mappaux = _mapper.Map<data.Usuarios, DataModels.Usuarios>(Usuarios);

            if (mappaux == null)
            {
                return NotFound();
            }

            return mappaux;
        }

        // PUT: api/Usuarios/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUsuarios(int id, DataModels.Usuarios Usuarios)
        {
            if (id != Usuarios.Idusuario)
            {
                return BadRequest();
            }

            try
            {
                //implementacion del automapper
                var mappaux = _mapper.Map<DataModels.Usuarios, data.Usuarios>(Usuarios);

                new Solution.BS.Usuario(_context).Update(mappaux);
            }
            catch (Exception ee)
            {
                if (!UsuariosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Usuarios
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<DataModels.Usuarios>> PostUsuarios(DataModels.Usuarios Usuarios)
        {
            //

            var mappaux = _mapper.Map<DataModels.Usuarios, data.Usuarios>(Usuarios);

            new Solution.BS.Usuario(_context).Insert(mappaux);

            return CreatedAtAction("GetUsuarios", new { id = Usuarios.Idusuario }, Usuarios);
        }

        // DELETE: api/Usuarios/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<DataModels.Usuarios>> DeleteUsuarios(int id)
        {
            var Usuarios = new Solution.BS.Usuario(_context).GetOneById(id);
            if (Usuarios == null)
            {
                return NotFound();
            }

            new Solution.BS.Usuario(_context).Delete(Usuarios);
            var mappaux = _mapper.Map<data.Usuarios, DataModels.Usuarios>(Usuarios);

            return mappaux;
        }

        private bool UsuariosExists(int id)
        {
            return (new Solution.BS.Usuario(_context).GetOneById(id) != null);
        }

    }
}
