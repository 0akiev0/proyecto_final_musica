﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Solution.DAL.EF;
using data = Solution.DO.Objects;
using AutoMapper;

namespace Musica.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CursosController : ControllerBase
    {
        private readonly SolutionDBContext _context;
        private readonly IMapper _mapper;

        public CursosController(SolutionDBContext context, IMapper mapper)
        {
            _context = context;
            this._mapper = mapper;
        }

        // GET: api/Cursos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DataModels.Cursos>>> GetCursos()
        {
            // carga de datos
            var aux = new Solution.BS.Curso(_context).GetAll().ToList();

            //implementacion del automapper
            var mappaux = _mapper.Map<IEnumerable<data.Cursos>, IEnumerable<DataModels.Cursos>>(aux).ToList();
            return mappaux.ToList();
        }

        // GET: api/Cursos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DataModels.Cursos>> GetCursos(int id)
        {
            var Cursos = new Solution.BS.Curso(_context).GetOneById(id);
            //implementacion del automapper
            var mappaux = _mapper.Map<data.Cursos, DataModels.Cursos>(Cursos);

            if (mappaux == null)
            {
                return NotFound();
            }

            return mappaux;
        }

        // PUT: api/Cursos/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCursos(int id, DataModels.Cursos Cursos)
        {
            if (id != Cursos.Idcurso)
            {
                return BadRequest();
            }

            try
            {
                //implementacion del automapper
                var mappaux = _mapper.Map<DataModels.Cursos, data.Cursos>(Cursos);

                new Solution.BS.Curso(_context).Update(mappaux);
            }
            catch (Exception ee)
            {
                if (!CursosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Cursos
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<DataModels.Cursos>> PostCursos(DataModels.Cursos Cursos)
        {
            //

            var mappaux = _mapper.Map<DataModels.Cursos, data.Cursos>(Cursos);

            new Solution.BS.Curso(_context).Insert(mappaux);

            return CreatedAtAction("GetCursos", new { id = Cursos.Idcurso }, Cursos);
        }

        // DELETE: api/Cursos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<DataModels.Cursos>> DeleteCursos(int id)
        {
            var Cursos = new Solution.BS.Curso(_context).GetOneById(id);
            if (Cursos == null)
            {
                return NotFound();
            }

            new Solution.BS.Curso(_context).Delete(Cursos);
            var mappaux = _mapper.Map<data.Cursos, DataModels.Cursos>(Cursos);

            return mappaux;
        }

        private bool CursosExists(int id)
        {
            return (new Solution.BS.Curso(_context).GetOneById(id) != null);
        }

    }
}
