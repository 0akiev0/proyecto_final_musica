﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Solution.DAL.EF;
using data = Solution.DO.Objects;
using AutoMapper;

namespace Musica.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventosController : ControllerBase
    {
        private readonly SolutionDBContext _context;
        private readonly IMapper _mapper;

        public EventosController(SolutionDBContext context, IMapper mapper)
        {
            _context = context;
            this._mapper = mapper;
        }

        // GET: api/Eventos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DataModels.Eventos>>> GetEventos()
        {
            // carga de datos
            var aux = new Solution.BS.Evento(_context).GetAll().ToList();

            //implementacion del automapper
            var mappaux = _mapper.Map<IEnumerable<data.Eventos>, IEnumerable<DataModels.Eventos>>(aux).ToList();
            return mappaux.ToList();
        }

        // GET: api/Eventos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DataModels.Eventos>> GetEventos(int id)
        {
            var Eventos = new Solution.BS.Evento(_context).GetOneById(id);
            //implementacion del automapper
            var mappaux = _mapper.Map<data.Eventos, DataModels.Eventos>(Eventos);

            if (mappaux == null)
            {
                return NotFound();
            }

            return mappaux;
        }

        // PUT: api/Eventos/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEventos(int id, DataModels.Eventos Eventos)
        {
            if (id != Eventos.Idevento)
            {
                return BadRequest();
            }

            try
            {
                //implementacion del automapper
                var mappaux = _mapper.Map<DataModels.Eventos, data.Eventos>(Eventos);

                new Solution.BS.Evento(_context).Update(mappaux);
            }
            catch (Exception ee)
            {
                if (!EventosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Eventos
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<DataModels.Eventos>> PostEventos(DataModels.Eventos Eventos)
        {
            //

            var mappaux = _mapper.Map<DataModels.Eventos, data.Eventos>(Eventos);

            new Solution.BS.Evento(_context).Insert(mappaux);

            return CreatedAtAction("GetEventos", new { id = Eventos.Idevento }, Eventos);
        }

        // DELETE: api/Eventos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<DataModels.Eventos>> DeleteEventos(int id)
        {
            var Eventos = new Solution.BS.Evento(_context).GetOneById(id);
            if (Eventos == null)
            {
                return NotFound();
            }

            new Solution.BS.Evento(_context).Delete(Eventos);
            var mappaux = _mapper.Map<data.Eventos, DataModels.Eventos>(Eventos);

            return mappaux;
        }

        private bool EventosExists(int id)
        {
            return (new Solution.BS.Evento(_context).GetOneById(id) != null);
        }

    }
}
