﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Musica.DataModels
{
    public class Matricula
    {
        public int Idmatricula { get; set; }
        public int? Idcurso { get; set; }
        public int? Idusuario { get; set; }

        public virtual Cursos IdcursoNavigation { get; set; }
        public virtual Usuarios IdusuarioNavigation { get; set; }
    }
}
