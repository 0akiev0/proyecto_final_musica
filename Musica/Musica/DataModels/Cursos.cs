﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Musica.DataModels
{
    public class Cursos
    {
        //public Cursos()
        //{
        //    Matricula = new HashSet<Matricula>();
        //}

        public int Idcurso { get; set; }
        public string Nombre { get; set; }
        public DateTime? Fecha { get; set; }

        //public virtual ICollection<Matricula> Matricula { get; set; }
    }
}
