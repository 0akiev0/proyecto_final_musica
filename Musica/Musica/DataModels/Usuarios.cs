﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Musica.DataModels
{
    public class Usuarios
    {
        //public Usuarios()
        //{
        //    Matricula = new HashSet<Matricula>();
        //}

        public int Idusuario { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Correo { get; set; }
        public decimal? Telefono { get; set; }
        public string Contrasena { get; set; }
        public string Rol { get; set; }

        //public virtual ICollection<Matricula> Matricula { get; set; }
    }
}
